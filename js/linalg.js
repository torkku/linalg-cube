export class Vector {
    constructor(x, y, z) {
        this.x = x,
        this.y = y,
        this.z = z
    }

    toVector() {
        return [this.x, this.y, this.z];
    }

    toArray() {
        return [[this.x],[this.y],[this.z]];
    }

    static fromArray(arr) {
        return new Vector(arr[0][0], arr[1][0], arr[2][0]);
    }
}

export class Vector2D {
    constructor(x, y) {
        this.x = x,
        this.y = y
    }

    toVector() {
        return [this.x, this.y];
    }

    toArray() {
        return [[x],[y]];
    }

    static fromArray(arr) {
        return new Vector(arr[0][0], arr[1][0]);
    }
}

export function rotate(vector, theta) {
    const matX = [
        [1, 0, 0],
        [0, Math.cos(theta), -Math.sin(theta)],
        [0, Math.sin(theta), Math.cos(theta)],
    ];
    const matY = [
        [Math.cos(theta), 0, Math.sin(theta)],
        [0, 1, 0],
        [-Math.sin(theta), 0, Math.cos(theta)],
    ];
    const matZ = [
        [Math.cos(theta),-Math.sin(theta), 0],
        [Math.sin(theta), Math.cos(theta), 0],
        [0, 0, 1],
    ];

    const mat = math.multiply(math.multiply(matX, matY), matZ);

    return Vector.fromArray(math.multiply(mat, vector.toArray()));
}

export function rotateX(vector, theta) {
    const mat = [
        [1, 0, 0],
        [0, Math.cos(theta), -Math.sin(theta)],
        [0, Math.sin(theta), Math.cos(theta)],
    ];

    return Vector.fromArray(math.multiply(mat, vector.toArray()));
}

export function rotateY(vector, theta) {
    const mat = [
        [Math.cos(theta), 0, Math.sin(theta)],
        [0, 1, 0],
        [-Math.sin(theta), 0, Math.cos(theta)],
    ];

    return Vector.fromArray(math.multiply(mat, vector.toArray()));
}

export function rotateZ(vector, theta) {
    const mat = [
        [Math.cos(theta),-Math.sin(theta), 0],
        [Math.sin(theta), Math.cos(theta), 0],
        [0, 0, 1],
    ];

    return Vector.fromArray(math.multiply(mat, vector.toArray()));
}


