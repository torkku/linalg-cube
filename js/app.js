import { Vector, Vector2D } from './linalg.js';
import { Cube, Scene } from './scene.js';

const cubeSize = 100;
const origo = new Vector(0, 0, 0);
const cube = new Cube(origo, cubeSize);

const width = 640;
const height = 480;
const scene = new Scene('cubeCanvas', width, height);
scene.addObject(cube);

let dx = width/2, dy = height/2;
scene.render(dx, dy);

animate(scene);

function animate(scene) {
    scene.render(dx, dy);
    window.requestAnimationFrame(animate.bind(this, scene));
}
