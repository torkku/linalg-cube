import { Vector, Vector2D, rotate, rotateX, rotateY, rotateZ } from './linalg.js';

export class Cube {
    constructor(center, size) {
        const d = size / 2;

        this.center = center;

        this.vertices = [
            new Vector(center.x - d, center.y - d, center.z + d),
            new Vector(center.x - d, center.y - d, center.z - d),
            new Vector(center.x + d, center.y - d, center.z - d),
            new Vector(center.x + d, center.y - d, center.z + d),
            new Vector(center.x + d, center.y + d, center.z + d),
            new Vector(center.x + d, center.y + d, center.z - d),
            new Vector(center.x - d, center.y + d, center.z - d),
            new Vector(center.x - d, center.y + d, center.z + d),
        ];

        this.calculateFaces();

    }

    calculateFaces() {
        this.faces = [
            [this.vertices[0], this.vertices[1], this.vertices[2], this.vertices[3]],
            [this.vertices[3], this.vertices[2], this.vertices[5], this.vertices[4]],
            [this.vertices[4], this.vertices[5], this.vertices[6], this.vertices[7]],
            [this.vertices[7], this.vertices[6], this.vertices[1], this.vertices[0]],
            [this.vertices[7], this.vertices[0], this.vertices[3], this.vertices[4]],
            [this.vertices[1], this.vertices[6], this.vertices[5], this.vertices[2]],
        ];
    }

    rotate(theta) {
        this.vertices = this.vertices.map((v) => rotate(v, theta) );
        this.calculateFaces();
    }
}

export class Scene {
    constructor(canvasElemId, width = 640, height = 480) {
        this.objs = [];
        this.width = width;
        this.height = height;
        this.rotationSpeed = 0.01;

        // where is the viewer sitting at?
        this.viewPort = new Vector(0, 0, -500);

        let canvas = document.getElementById(canvasElemId);
        canvas.width = this.width;
        canvas.height = this.height;
        canvas.style.position = 'absolute';
        canvas.style.backgroundColor = 'black';
        canvas.style.zIndex = 1;

        this.ctxt = canvas.getContext('2d');
        this.ctxt.lineWidth = 1;
        this.ctxt.strokeStyle = 'rgba(255, 255, 255, 0.3)';
    }

    isFaceVisible(face) {
        const normal = this.surfaceNormal(face);
        const dotProduct = math.dot(normal, this.viewPort.toVector());
        return dotProduct < 0;
    }

    surfaceNormal(face) {
        return math.cross(face[0].toVector(), face[1].toVector());
    }

    addObject(o) {
        this.objs.push(o);
    }

    render(dx, dy) {

        this.ctxt.clearRect(0, 0, this.width, this.height);
        this.ctxt.fillStyle = 'rgba(255, 0, 0, 0.3)';

        this.objs.forEach((o) => {

            o.rotate(this.rotationSpeed);

            o.faces
                .filter((face) => this.isFaceVisible(face))
                .forEach((face) => {

                // first vertex
                const vertex = face[0];
                const projection = this.project(vertex);
                this.ctxt.beginPath();
                this.ctxt.moveTo(projection.x + dx, -projection.y + dy);

                // rest of the vertices
                const rest = face.slice(1, face.size);
                rest.forEach((v) => {
                    const P = this.project(v);
                    this.ctxt.lineTo(P.x + dx, -P.y + dy);
                });

                this.ctxt.closePath();
                this.ctxt.stroke();
                this.ctxt.fill();
            })
        })
    }

    // orthographic projection, just drop the z coordinate
    project(vertex) {
        return new Vector2D(vertex.x, vertex.y)
    }
}

